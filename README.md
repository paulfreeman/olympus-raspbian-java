# olympus-raspbian-java
A tutorial and sample of how to control the Olympus Air from a Raspberry Pi
 
This describes how to develop your own apps using Java and Groovy to control the Olympus Air 01 on a Raspberry Pi B+ v2

The Olympus Camera SDK contains easy to use SDK's for iOS and Android for making mobile apps, but it also includes a 'generic' API using HTTP documented in a very complex looking protocol document.  In reality its easy to develop apps for this API using a variety of languages, you aren't limited to the use of iOS and Android. This makes the Air 01 a great camera module for hackers. 
  
For those who don't know, Groovy is a scripting language that makes developing Java programs fast and easy,  in particular it makes it very easy to develop programs that can talk to HTTP interfaces such as that used by the Olympus Air 01 or the Sony Camera SDK which is also an HTTP API. 

A few things need to be installed before we can write some basic software to connect to the Camera. 

# Installing Java

Install the Java development kit
http://www.rpiblog.com/2014/03/installing-oracle-jdk-8-on-raspberry-pi.html

# Installing the Groovy Scripting language 

The best way is to install the Groovy environment manager GVM, this only needs curl and bash which are already on the Rasp Pi. The home page for gvm is http://gvmtool.net/

First get gvm

curl -s get.gvmtool.net | bash

Then follow the instructions in the prompt, open a new terminal and run

gvm help 

to ensure its all working. 



Then 

gvm install groovy 

and wait till the download has completed 

You can then run your first groovy script,  the -e flag executes the following string as a groovy script

groovy -e "println 'Hello from the Groovy world'"

This takes a while to return as there is an overhead to starting up Groovy in this mode, however our final program will be compiled and will be plenty quick enough for what we need to do. 

Now we need to install the gradle build tool. This will help us load the libraries that make connecting to the camera quick and easy. 

gvm install gradle

After this is complete test its working correctly with the following 

gradle tasks

The Pi is only a small computer so we can get things to go a bit faster by running Gradle as a daemon so its always there in the background. We won't need this when running any apps for real, but during development this can help. 

create a file called gradle.properties in your favorite editor.  I use vi (ugh)

vi ~/.gradle/gradle.properties

Then add the line 

org.gradle.daemon=true 

Then if you run gradle tasks again it will keep running in the background speeding up future runs of any commands 

The installation process is now over and its time to configure the libraries we need to make Groovy scripts talk to the camera 


# Configuring libraries that make communicating with the camera easy

Create a directory where you will create your project 

mkdir MyGroovyOly
cd MyGroovyOly 

Then create a build.gradle file with the following text 

apply plugin: 'java'
apply plugin: 'groovy'

repositories {
   mavenCentral()
}

dependencies {
   compile 'org.codehaus.groovy:groovy-all:2.4.4'
}



The great thing about using this is that with Gradle an implicit project/directory 
structure is assumed, so we can easily tell where our Java and 
Groovy code should go under our project directory. 

This structure is described here in the section 2.43 Project Layout

https://docs.gradle.org/current/userguide/groovy_plugin.html


We don't need all of it for a quick hack, so lets just create 
the directory structure we need 

mkdir src/main
mkdir src/main/groovy
mkdir src/main/java
mkdir src/main/resources

# First program 

To make sure everything is working we'll create a quick HelloWorld

Create a file called Main.groovy in src/main/groovy with the following 
code 

class Main {

  public static void main(String [] args) {
     println "Hello from the Groovy World"
  }

}

Once you have this, run the build from the root directory of your 
project 

This will take a while the first time because library dependencies must 
be downloaded from the maven repositories on the internet. The beauty 
of the gradle system is that everything needed is automatically 
downloaded. 

Once this is finished you can examine the results in the
file browser and run your first program 

java -classpath 'build/lib/OlyRasp.jar' Main

Hello Groovy World 

With this completed we are ready to start communicating with the camera for the
first time. 





















